import React, { Component } from 'react';
import './Form.scss';

const INITIAL_STATE = {
    nombre: "",
    movie: "",
    description: "",
    imageUrl: "",
}


class Form extends Component {
    state = INITIAL_STATE;

    handleFormSubmit = (ev) => {
        //Cuando se envíe el formulario, aquí sucede la magia.
        ev.preventDefault();
        this.props.addPersonaje(this.state);
        this.setState(INITIAL_STATE);
    }

    //Cuando cambien el Input se ejecuta:
    handleInputChange = (ev) => {
        // 1.Coger los campos name y value
        // 2. Actualizar el estado
        const { name, value } = ev.target;

        this.setState({
            [name]: value
        });
    }

    render() {
        console.log(this.props);
        return (
            <form onSubmit={this.handleFormSubmit}>
                <label>
                    <p>Nombre</p>
                    <input type="text" name="nombre" value={this.state.name} onChange={this.handleInputChange} />
                </label>

                <label>
                    <p>Película o Serie</p>
                    <input type="text" name="movie" value={this.state.movie} onChange={this.handleInputChange} />
                </label>

                <label>
                    <p>Descripción</p>
                    <textarea className="textarea" type="text" name="description" value={this.state.description} onChange={this.handleInputChange} />
                </label>

                <label>
                    <p>Imagen</p>
                    <input type="text" name="imageUrl" value={this.state.imageUrl} onChange={this.handleInputChange} />
                </label>

                <div>
                    <button type="submit">Enviar Formulario</button>
                </div>
            </form>
        )
    }
}

export default Form;