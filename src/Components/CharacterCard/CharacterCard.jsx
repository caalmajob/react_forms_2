import React, { Component } from 'react';
import './CharacterCard.scss';


class CharacterCard extends Component {

    render() {
        return (
            <div className="character-card">
            <h3>Nombre:</h3><span> {this.props.personaje.nombre}</span>
            <img src={this.props.personaje.imageUrl} alt="character"/>
            <h3>Película: </h3><span>{this.props.personaje.movie}</span>
            <h3>Descripción del personaje: </h3><span>{this.props.personaje.description}</span>
            
            </div>
        )
    }
}

export default CharacterCard;