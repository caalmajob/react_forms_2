import React, { Component } from 'react';
import Form from './Components/Form';
import CharacterCard from './Components/CharacterCard';
import './App.scss';

const personajesDeLocalStorage =  JSON.parse(localStorage.getItem('listaDePersonajes'));

class App extends Component {
  state = {
    characterList: personajesDeLocalStorage || []
  }

  addCharacter = (character) => {
    this.setState({
      characterList: [...this.state.characterList, character]
    }, () => this.storeCharacterList(this.state.characterList));
  }

//Local Storage
  storeCharacterList = (characterList) => {
    localStorage.setItem('listaDePersonajes', JSON.stringify(characterList));
  }

  render() {
    return (
      <div className="App">
        <Form addPersonaje={this.addCharacter}/>
        

        <div className="character-container">
        {this.state.characterList.map(character => {
          return <CharacterCard key={character} personaje={character}/>;
        })}
        </div>
       
      </div>
    );
  }
}

export default App;
